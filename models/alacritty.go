package models

// Alacritty main file configuration
type Alacritty struct {
	Env struct {
		Term string
	}
	Import            []string // The important field is this
	BackgroundOpacity int
	SaveClipboard     bool
	LiveReload        bool
}
