package settings

type FontsFile struct {
	Fonts                        Fonts `mapstructure:"font"`
	DrawBoldTextWithBrightColors bool  `mapstructure:"draw_bold_text_with_bright_colors"`
}

// It represents the whole configuration file
type Fonts struct {
	Normal      FontStyle
	Bold        FontStyle
	Italic      FontStyle
	Bold_Italic FontStyle
	Size        int `mapstructure:"size"`
}

// It represents a single font style configuration
type FontStyle struct {
	Family string
	Style  string
}
