package models

type ViperFile struct {
	Name string
	Path string
	Type string
}
