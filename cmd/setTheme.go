package cmd

import (
	"fmt"
	"path"

	"github.com/spf13/cobra"
	"gitlab.com/4strodev/gocritty/models"
)

var SetTheme = &cobra.Command{
	Use:   "set",
	Short: "Set alacritty theme.",
	Long:  `Set alacritty theme. This theme needs to be 'installed' before`,
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) <= 1 {
			return nil
		}
		return fmt.Errorf("Invalid number of arguments: %v args recived, %v or %v args expected", len(args), 0, 1)
	},
	Run: setTheme,
}

// Return the full path of the current alacritty theme
func GetThemePath() string {
	// Getting file name
	fileName := fmt.Sprintf("%s.%s", ThemeFile.Name, ThemeFile.Type)
	// Joining and making the full path
	themePath := path.Join(ThemeFile.Path, fileName)
	return themePath
}

// Set the theme that user specify or the default theme
func setTheme(cmd *cobra.Command, args []string) {
	InitViper(args)
	// unmarshalling current config file
	configData := new(models.Alacritty)
	ViperConfig.Unmarshal(&configData)

	// TODO Search if can use struct to write config
	// changing theme
	themePath := GetThemePath()
	configData.Import[0] = themePath
	ViperConfig.Set("import", configData.Import)

	// Capitalizing environment variable $TERM
	ViperConfig.Set("env", map[string]string{
		"TERM": ViperConfig.GetString("env.term"),
	})

	// writing to file
	ViperConfig.WriteConfig()
}
