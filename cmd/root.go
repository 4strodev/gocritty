package cmd

import (
	"github.com/spf13/cobra"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "gocritty",
	Short: "Configure alacritty from CLI",
	Long: `gocritty is a CLI program made in go that allows you to configure your terminal (alacritty) from CLI.

Example Use:

    Change your current theme:
        - gocritty theme set <theme>
    Change your font:
        - gocritty font set <font>`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	// Adding subcommands
	rootCmd.AddCommand(ThemeCmd)
	rootCmd.AddCommand(FontCmd)
}
