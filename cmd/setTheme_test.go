package cmd

import (
	"fmt"
	"regexp"
	"testing"
)

func TestGetThemePath(t *testing.T) {
	reg, _ := regexp.CompilePOSIX(`/home/[[:alpha:]]+/.config/alacritty/themes/[[:alpha:]]+.y[[:alpha:]]?ml`)

	if match := reg.MatchString(GetThemePath()); !match {
		t.Log("Path incorrect")
		t.Log(fmt.Sprintf("Recived path %s", GetThemePath()))
		t.Fail()
	}
}
