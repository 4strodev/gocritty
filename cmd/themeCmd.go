package cmd

import (
	"fmt"
	"log"
	"os"
	"path"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/4strodev/gocritty/models"
)

var (
	ConfigFile = models.ViperFile{
		Name: "alacritty",
		Path: path.Join(os.Getenv("HOME"), ".config/alacritty"), // /home/user/.config/alacritty
		Type: "yaml",
	}
	ThemeFile = models.ViperFile{
		Name: "default",
		Path: path.Join(os.Getenv("HOME"), ".config/alacritty/themes"), // /home/user/.config/alacritty
		Type: "yaml",
	}
	ViperTheme  = viper.New()
	ViperConfig = viper.New()
)
var ThemeCmd = &cobra.Command{
	Use:   "theme",
	Short: "theme actions for alacritty",
	Long:  `Set and list installed themes. Also you can get current theme`,
	Run:   showTheme,
}

// Load config files into viper
func InitViper(args []string) {
	// If an arguments is passed it will be the theme to load
	if len(args) == 1 {
		ThemeFile.Name = args[0]
	}

	// Setting file path and file type (yaml, toml, json, etc.) in this case YAML
	ViperTheme.SetConfigName(ThemeFile.Name)
	ViperTheme.SetConfigType(ThemeFile.Type)
	ViperTheme.AddConfigPath(ThemeFile.Path)

	// Checking if file exists
	if err := ViperTheme.ReadInConfig(); err != nil {
		//fmt.Printf("Error reading theme file: %v\n", err)
		//os.Exit(1)
		log.Fatalf("Error reading config file: %v\n", err)
	}

	// Setting file path and file type (yaml, toml, json, etc.) in this case YAML
	ViperConfig.SetConfigName(ConfigFile.Name)
	ViperConfig.SetConfigType(ConfigFile.Type)
	ViperConfig.AddConfigPath(ConfigFile.Path)

	// Checking if file exists
	if err := ViperConfig.ReadInConfig(); err != nil {
		//fmt.Printf("Error reading config file: %v\n", err)
		//os.Exit(1)
		log.Fatalf("Error reading config file: %v\n", err)
	}
}

// Set flags and subcommands
func init() {
	// Add subcommands:  <09-11-21, astro> //
	ThemeCmd.AddCommand(SetTheme)
}

func showTheme(cmd *cobra.Command, args []string) {
	// load config files
	InitViper(args)
	configData := new(models.Alacritty)
	ViperConfig.Unmarshal(configData)

	// get file nime ex: onedark.yaml
	fileName := path.Base(configData.Import[0])

	// split and show the first string -> [onedark] <- [yaml]
	fmt.Println(strings.Split(fileName, ".")[0])
}
