package cmd

import (
	"fmt"
	"log"
	"os"
	"path"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/4strodev/gocritty/models"
	"gitlab.com/4strodev/gocritty/models/settings"
)

var FontCmd = &cobra.Command{
	Use:   "font",
	Short: "font actions for alacritty",
	Long: `Set and list current fonts of your terminal. You can set 4 different styles:
    - regular
    - bold
    - italic
    - bold and italic`,
	Run: showFont,
}

var (
	FontFile = models.ViperFile{
		Name: "fonts",
		Path: path.Join(os.Getenv("HOME"), ".config/alacritty/settings"),
		Type: "yaml",
	}
	Font = viper.New()
)

// Setting flags commands and subcommands
// Also initialating viper
func init() {
	// initialating viper
	Font.SetConfigName(FontFile.Name)
	Font.SetConfigType(FontFile.Type)
	Font.AddConfigPath(FontFile.Path)
}

func showFont(cmd *cobra.Command, args []string) {
	if err := Font.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file: %v\n", err)
	}

	// creating struct o save font config
	fontsStruct := new(settings.FontsFile)

	// Putting values into struct
	Font.Unmarshal(&fontsStruct)

	// Formating message
	message := fmt.Sprintf(`Current font configuration:
        Normal font:      %s
        Bold font:        %s
        Italic font:      %s
        Bolt_Italic Font: %s

        Font size:        %d
`,
		fontsStruct.Fonts.Normal.Family,
		fontsStruct.Fonts.Bold.Family,
		fontsStruct.Fonts.Italic.Family,
		fontsStruct.Fonts.Bold_Italic.Family,
		fontsStruct.Fonts.Size)

	// Showing message
	fmt.Println(message)
}
