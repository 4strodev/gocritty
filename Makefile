all: build

build:
	go build -o bin/ .

clean:
	go clean
